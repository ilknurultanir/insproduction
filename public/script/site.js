$(document).ready(function () {

    $("form#apiQuery select#note").on("change", function () {
        var uri = "insproduction.herokuapp.com/api?note=" + String($(this).val())+ "&scale=majorScale"
        var shortUri = "/api?note=" + String($(this).val())+ "&scale=majorScale";
        $("a.api").text(uri);
        $("a.api").attr("href",shortUri);
    })

    $("form#apiQuery select#scale").on("change", function () {
        var uri = "insproduction.herokuapp.com/api?note=C&scale=" + String($(this).val());
        var shortUri = "/api?note=C&scale=" + String($(this).val());
        $("a.api").text(uri);
        $("a.api").attr("href",shortUri);
    })

});