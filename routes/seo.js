var express = require("express"),
    router = express.Router(),
    path = require('path'),
    SitemapGenerator = require('sitemap-generator');


// create generator
const generator = SitemapGenerator('http://insproduction.herokuapp.com', {
    ignoreHreflang: true,
    maxDepth: 0,
    filepath: path.join('..','views','sitemap.xml'),
});

generator.start();

generator.on("done",()=>{
    // console.log("done");
});

router.get('/sitemap.xml', function (req, res) {
    res.header('Content-Type', 'application/xml');
    res.render("./sitemap.xml");
});

router.get('/robots.txt', function (req, res) {
    res.header('Content-Type', 'text/plain');
    res.send('User-agent: * \nAllow: \nSitemap: /sitemap.xml');
});

module.exports = router;