const express = require("express");
const router = express.Router();

router.get("/blog/:subject",(req,res)=>{
    res.render("blog/blogContent",{content:`${req.params.subject}`});
});


module.exports = router;