const express = require("express"),
  router = express.Router(),
  includedNotes = require("../funcs/variables/notes"),
  includedScales = require("../funcs/variables/scales"),
  includedIntervals = require("../funcs/variables/intervals"),
  includedChords = require("../funcs/variables/chords");

const notesData = includedNotes.notes;
const scalesData = includedScales;
const intervalsData = includedIntervals;
const chordsData = includedChords;

var { analyzeChords } = require("../funcs/analyzeChords"),
  { analyzeScale } = require("../funcs/analyzeScale");

router.get("/", (req, res) => {
  res.render("query", { scales: scalesData, notes: notesData });
});

router.post("/result", (req, res) => {
  const {
    quest: { note, scale },
  } = req.body;

  res.render("result", {
    result: {
      note: note,
      scale: scale,
      notes: ["C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"],
      result: analyzeScale(note, scale),
      chords: analyzeChords(analyzeScale(note, scale)),
    },
  });
});

module.exports = router;
