const express = require("express"),
    router = express.Router(),
    includedScales = require("../funcs/variables/scales"),
    includedNotes = require("../funcs/variables/notes");


var chords = require("../funcs/analyzeChords"),
    scale = require("../funcs/analyzeScale");

router.get("/api", (req, res) => {
    res.json({
        chords: chords.analyzeChords(scale.analyzeScale(req.query.note, req.query.scale))
    });
});

router.get("/api/doc", (req, res) => {
    res.render("api/doc", {
        scales: includedScales,
        notes: includedNotes.notes
    });
});

module.exports = router;