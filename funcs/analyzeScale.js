const express = require("express"),
    includedNotes = require("./variables/notes"),
    includedScales = require("./variables/scales"),
    includedIntervals = require("./variables/intervals"),
    includedChords = require("./variables/chords");


const notes = includedNotes.notes;
const scales = includedScales.scales;
const intervals = includedIntervals.intervals;
const chords = includedChords.chords;

module.exports.analyzeScale = function (note, scale) {
    var result = [];
    var noteIndex = notes.indexOf(note);
    if (note !== undefined && scale !== undefined && notes.indexOf(note) > -1) {
        scales[scale].map.forEach(elem => {
            result.push(notes[(noteIndex + intervals[elem].index)%12]);
        });
    } else {
        result = null;
    }
    return result;
};