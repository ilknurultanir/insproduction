const express = require("express");

module.exports.scales = {
    majorScale: {
        name: "Major Scale",
        map: ["P1", "M2", "M3", "P4", "P5", "M6", "M7", "P8"]
    },
    minorPentatonic: {
        name:"Minor Pentatonic Scale",
        map:["P1", "m3", "P4", "P5", "m7"]
    },
    majorPentatonic: {
        name:"Major Pentatonic Scale",
        map:["P1", "M2", "M3" ,"P5" ,"M6"]
    },
    majorBlues: {
        name:"Major Blues",
        map:["P1","M2","m3","M3","P5","M6"]
    },
    minorBlues: {
        name:"Minor Blues",
        map:["P1","m3","P4","d5","P5","m7"]
    }
};