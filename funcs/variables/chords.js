const express = require("express"),
    includedNotes = require("./notes"),
    includedIntervals = require("./intervals"),
    notes = includedNotes.notes,
    intervals = includedIntervals.intervals;


module.exports.chords = {
    major: {
        maj: {
            name:"M",
            interval: ["P1", "M3", "P5"]
        },
        maj6:{
            name:"M6",
            interval: ["P1", "M3", "P5","M13"]
        },
        maj7:{
            name:"M7",
            interval: ["P1", "M3", "P5","M7"]
        },
        maj9:{
            name:"M9",
            interval: ["P1", "M3", "P5","M7","M9"]
        },
        maj13:{
            name:"M13",
            interval: ["P1", "M3", "P5","M7","M9","M13"]
        }

    },
    minor:{
        min:{
            name:"m",
            interval: ["P1", "m3", "P5"]
        },
        min6:{
            name:"m6",
            interval: ["P1", "m3", "P5","M13"]
        },
        min7:{
            name:"m7",
            interval: ["P1", "m3", "P5","m7"]
        },
        min7Flat5:{
            name:"m7b5",
            interval: ["P1", "m3", "P5","m7"]
        },
        min9:{
            name:"m9",
            interval: ["P1", "m3", "P5","m7","M9"]
        },
        // min11:{
        //     name:"m11",
        //     interval: ["P1", "m3", "P5","m7","M11"]
        // },
        min13:{
            name:"m11",
            interval: ["P1", "m3", "P5","m7","M9","P11","M13"]
        },
    },
    aug:{
        aug:{
            name:"aug",
            interval: ["P1", "m3", "P5"]
        }
    },
    dim:{
        dim:{
            name:"dim",
            interval: ["P1", "m3", "d5"]
        },
    },
    // dominantSeven:{
    //     dominantSeven:{
    //         name:"7",
    //         interval: ["P1", "M3", "M5","m7"]
    //     }
    // },
    // sixth:{
    //     sixth:{
    //         name:"6",
    //         interval: ["P1", "M3", "M5","M13"]
    //     }
    // },
    sus:{
        sus2:{
            name:"sus2",
            interval: ["P1", "M2", "P5"]
        },
        sus4:{
            name:"sus4",
            interval: ["P1", "P4", "d5"]
        }
    },
    add:{
        add9:{
            name:"add9",
            interval: ["P1", "M3", "d5","M9"]
        }
    }
};

