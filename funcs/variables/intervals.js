const express = require("express");

module.exports.intervals = {
    P1: {
        index: 0,
        name: "Root",
        short: "P1",
        alternativeName: "",
        augOrDim: "Diminished second",
        augOrDimShort: "d2"
    },
    m2: {
        index: 1,
        name: "Minor Second",
        short: "m2",
        alternativeName: "Semitone",
        augOrDim: "Augmented unison",
        augOrDimShort: "A1"
    },
    M2: {
        index: 2,
        name: "Major Second",
        short: "M2",
        alternativeName: "Tone",
        augOrDim: "Diminished third",
        augOrDimShort: "d3"
    },
    m3: {
        index: 3,
        name: "Minor Third",
        short: "m3",
        alternativeName: "",
        augOrDim: "Augmented second",
        augOrDimShort: "A2"
    },
    M3: {
        index: 4,
        name: "Major Third",
        short: "M3",
        alternativeName: "",
        augOrDim: "Diminished fourth",
        augOrDimShort: "d4"
    },
    P4: {
        index: 5,
        name: "Perfect fourth",
        short: "P4",
        alternativeName: "",
        augOrDim: "Augmented third",
        augOrDimShort: "A3"
    },
    d5: {
        index: 6,
        name: "Diminished fifth",
        short: "d5",
        alternativeName: "Tritone",
        augOrDim: "Augmented fourth",
        augOrDimShort: "A4"
    },
    P5: {
        index: 7,
        name: "Perfect fifth",
        short: "P5",
        alternativeName: "",
        augOrDim: "Diminished sixth",
        augOrDimShort: "d6"
    },
    m6: {
        index: 8,
        name: "Minor sixth",
        short: "m6",
        alternativeName: "",
        augOrDim: "Augmented fifth",
        augOrDimShort: "A5"
    },
    M6: {
        index: 9,
        name: "Major sixth",
        short: "M6",
        alternativeName: "",
        augOrDim: "Diminished seventh",
        augOrDimShort: "d7"
    },
    m7: {
        index: 10,
        name: "Minor seventh",
        short: "m7",
        alternativeName: "",
        augOrDim: "Augmented sixth",
        augOrDimShort: "A6"
    },
    M7: {
        index: 11,
        name: "Major seventh",
        short: "M7",
        alternativeName: "",
        augOrDim: "Diminished octave",
        augOrDimShort: "d8"
    },
    P8: {
        index: 12,
        name: "Perfect octave",
        short: "P8",
        alternativeName: "",
        augOrDim: "Augmented seventh",
        augOrDimShort: "A7"
    },
    m9: {
        index: 13,
        name: "Minor Ninth",
        short: "m9",
        octave: true,
        octaveName : "Minor Second",
        alternativeName: "",
        augOrDim: "",
        augOrDimShort: ""
    },
    M9: {
        index: 14,
        name: "Major Ninth",
        short: "M9",
        octave: true,
        octaveName:  "Major Second",
        alternativeName: "",
        augOrDim: "",
        augOrDimShort: ""
    },
    m10: {
        index: 15,
        name: "Minor Tenth",
        short: "m10",
        octave: true,
        octaveName : "Minor Third",
        alternativeName: "",
        augOrDim: "",
        augOrDimShort: ""
    },
    M10: {
        index: 16,
        name: "Major Tenth",
        short: "M10",
        octave: true,
        octaveName : "Major Third",
        alternativeName: "",
        augOrDim: "",
        augOrDimShort: ""
    },
    P11: {
        index: 17,
        name: "Perfect eleventh",
        short: "P11",
        octave: true,
        octaveName : "Perfect fourth",
        alternativeName: "",
        augOrDim: "",
        augOrDimShort: ""
    },
    A11: {
        index: 18,
        name: "Augmented eleventh",
        short: "A11",
        octave: true,
        octaveName : "Tritone",
        alternativeName: "",
        augOrDim: "",
        augOrDimShort: ""
    },
    P12: {
        index: 19,
        name: "Perfect twelfth",
        short: "P12",
        octave: true,
        octaveName : "Perfect fifth",
        alternativeName: "",
        augOrDim: "",
        augOrDimShort: ""
    },
    m13: {
        index: 20,
        name: "Minor thirteenth",
        short: "m13",
        octave: true,
        octaveName : "Minor sixth",
        alternativeName: "",
        augOrDim: "",
        augOrDimShort: ""
    },
    M13: {
        index: 21,
        name: "Major thirteenth",
        short: "M13",
        octave: true,
        octaveName : "Major sixth",
        alternativeName: "",
        augOrDim: "",
        augOrDimShort: ""
    },
    m14: {
        index: 22,
        name: "Minor fourteenth",
        short: "m14",
        octave: true,
        octaveName : "Minor seventh",
        alternativeName: "",
        augOrDim: "",
        augOrDimShort: ""
    },
    M14: {
        index: 23,
        name: "Major Fourteenth",
        short: "M14",
        octave: true,
        octaveName : "Major seventh",
        alternativeName: "",
        augOrDim: "",
        augOrDimShort: ""
    },
    P16: {
        index: 24,
        name: "Double Octave",
        short: "P16",
        octave: true,
        octaveName : "Perfect octave",
        alternativeName: "",
        augOrDim: "Augmented seventh",
        augOrDimShort: "A7"
    },



};