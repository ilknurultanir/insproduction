const express = require("express"),
    includedNotes = require("./variables/notes"),
    includedIntervals = require("./variables/intervals"),
    notes = includedNotes.notes,
    intervals = includedIntervals.intervals;

module.exports.generateChord = function (note, interval) {
    var result = [];
    var noteIndex = notes.indexOf(note);
    interval.forEach((degree) => {
        result.push(`${notes[(noteIndex + intervals[degree].index) % 12]}`);
    });
    return result;
};