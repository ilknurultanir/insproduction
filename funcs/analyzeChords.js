const express = require("express"),
    generateChords = require("./chordGenerator"),
    includedNotes = require("./variables/notes"),
    includedScales = require("./variables/scales"),
    includedIntervals = require("./variables/intervals"),
    includedChords = require("./variables/chords");


const notes = includedNotes.notes;
const scales = includedScales;
const intervals = includedIntervals;
const chords = includedChords;


module.exports.analyzeChords = function (scaleNotes) {
    var objects = [];

    if (scaleNotes !== null && typeof scaleNotes !== Object) {
        scaleNotes.forEach(function (note) {
            objects.push({
                note: note,
                index: scaleNotes.indexOf(note)
            });
        });

        Object.keys(chords.chords).forEach(function (key) {
            Object.keys(chords.chords[key]).forEach(function (downkey) {
                scaleNotes.forEach(function (scaleNote) {
                    if (generateChords.generateChord(scaleNote, chords.chords[key][downkey].interval).every(val => scaleNotes.includes(val))) {
                        for (var i = 0; i < objects.length; i++) {
                            if (objects[i].note == scaleNote) {
                                var myobj = {};
                                myobj[downkey] =[ generateChords.generateChord(scaleNote, chords.chords[key][downkey].interval), [chords.chords[key][downkey].name], chords.chords[key][downkey].interval];
            
                                if (objects[i].chords) {
                                    objects[i].chords = Object.assign(objects[i].chords, myobj);
                                } else {
                                    objects[i].chords = myobj;
                                }

                            }
                        }
                    }
                });
            });
        });
    } else {
        delete objects.chords;
        objects.error = "No parameter was sent";
    }

    return objects;
};