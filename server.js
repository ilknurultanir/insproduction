const express = require("express"),
  path = require("path"),
  app = express();

var indexRoutes = require("./routes/index"),
  blogRoutes = require("./routes/blog"),
  seoRoutes = require("./routes/seo"),
  apiRoutes = require("./routes/api");

app.set("view engine", "ejs");
app.set("trust proxy", true);
app.set("views", path.join(__dirname, "views"));
app.use(express.json())
app.use(express.static(__dirname + "/public"));

app.use(indexRoutes);
app.use(blogRoutes);
app.use(apiRoutes);
// app.use(seoRoutes);

app.get("*", (req, res) => {
  res.redirect("/");
});

const port = process.env.PORT || 3333;

app.listen(port, () => {
  console.log(`Server runs on ${port}`);
});
